JetBrains License Server for Java
======================================================================

What's this?
---------------------------------------------------------------------
A license server for JetBrains products.

***
Thank ilanyu

NOTE: This is provided for educational purposes only. Please support genuine.
***
Setup
----------------------------------------------------------------------
Run:
```
cd /path/to/project
mvn compile 
mvn exec:java -Dexec.mainClass="com.vvvtimes.server.MainServer" 
```
default port is 8081.

Support
----------------------------------------------------------------------

IntelliJ IDEA>=7.0

ReSharper>=3.1

ReSharper>=Cpp 1.0

dotTrace>=5.5

dotMemory>=4.0

dotCover>=1.0

RubyMine>=1.0

PyCharm>=1.0

WebStorm>=1.0

PhpStorm>=1.0

AppCode>=1.0

CLion>=1.0

GoLand>=1.0

Ban license server address list
----------------------------------------------------------------------
 1. http://\*.lanyus.com:\*
 2. http://\*.qinxi1992.cn:\*
 3. http://127.0.0.1:1017

Similar products
----------------------------------------------------------------------

  * Go
    * <https://bitbucket.org/echizenryoma/jetbrains-license-server/downloads/>
  * C
    * <https://coding.net/u/privatelo/p/lightjbl>
  * Python
    * <https://gitee.com/gsls200808/JetBrainsLicenseServerforPython> 
  * PHP
    * <https://gitee.com/gsls200808/JetBrainsLicenseServerforPHP> 